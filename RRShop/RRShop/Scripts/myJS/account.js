﻿$(".top-link > .box-right #logout").on("click", function () {
    $("#logoff").submit();
});

$(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    todayBtn: "linked",
    language: "vi",
    autoclose: true,
    todayHighlight: true,
    toggleActive: true
}).removeAttr("data-val-date");

$(".ui.dropdown").dropdown();