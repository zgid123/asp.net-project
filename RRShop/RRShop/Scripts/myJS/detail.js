﻿$(".product-collateral > .product-tabs > li > a").on("click", function () {
    $(".product-collateral > .product-tabs > li").removeClass("active");
    $(".product-collateral > .product-tabs-content").css("display", "none");

    $($(this).attr("tag")).css("display", "");
    $(this).parent().addClass("active");
});

$(".cloud-zoom-gallery").on("click", function () {
    $("#ma-zoom1 > img").attr("src", $(this).children("img").attr("src"));
});