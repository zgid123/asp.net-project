﻿$("#oldAddress").prop("checked", true);

$('.ui.accordion').accordion();

$(".ui.checkbox.radio > input").on("click", function () {
    $("#error").text("");

    $("input[name='Address']").hide();

    $($(this).attr("tag")).show();
});

$(".ui.checkbox.radio > label").on("click", function () {
    $(this).prev().prop("checked", true);

    $("input[name='Address']").hide();

    $($(this).prev().attr("tag")).show();

    $("#error").text("");
});

$("#next").on("click", function () {
    if ($(".steps").find("div.active").attr("tag") !== "#segment_3") {
        var check = $("#newAddress").is(":checked");

        if (check) {
            $("#Address").val($("#txtNewAddress").val());
        } else {
            $("#Address").val($("#txtOldAddress").val());
        }

        $("#shipAddress").text($("#Address").val());

        if (check && $("#txtNewAddress").val() === "") {
            $("#txtNewAddress").next().text($("#txtNewAddress").attr("data-val-required"));
        } else {
            $("div[id*='segment_'").hide();
            var currentStep = $(".steps").find("div.active");

            currentStep.next().removeClass("disabled").addClass("active");
            currentStep.removeClass("active").addClass("completed");

            var nextTag = currentStep.next().attr("tag");
            $(nextTag).show();

            $("#segmentButton button[tag='" + nextTag + "'").show();
            $(this).hide();
        }
    }
});

$("#previous").on("click", function () {
    if ($(".steps").find("div.active").attr("tag") !== "#segment_2") {
        $("div[id*='segment_'").hide();
        var currentStep = $(".steps").find("div.active");

        currentStep.prev().removeClass("completed").addClass("active");
        currentStep.removeClass("active").addClass("disabled");

        var prevTag = currentStep.prev().attr("tag");
        $(prevTag).show();

        $("#segmentButton button[tag='" + prevTag + "'").show();
        $("#segmentButton button[tag='" + currentStep.attr("tag") + "'").hide();
        $("#back").hide();
    }
});

$("#finish").on("click", function () {
    if ($(".steps").find("div.active").attr("tag") === "#segment_3") {
        $.post(urlCheckout, { "address": $("#Address").val() }, function (data) {
            $("div[id*='segment_'").hide();
            var currentStep = $(".steps").find("div.active");

            currentStep.next().removeClass("active").addClass("completed");
            currentStep.removeClass("active").addClass("completed");

            var nextTag = currentStep.next().attr("tag");
            $(nextTag).show();

            $("#segmentButton button[tag='" + nextTag + "'").show();
            $("#segmentButton button[tag='" + currentStep.attr("tag") + "'").hide();
            $("#back").show();

            $("#message").text(data);
        });
    }
});