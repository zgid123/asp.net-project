﻿$(document).ready(function () {
    /*button decrease*/
    $(".qty-spinner > .btn-decrease").on("mouseenter", function () {
        $(this).stop(true, true).animate({
            "margin-right": "-1px"
        }).css({
            "text-align": "center",
            "padding-left": "7px"
        });
    });

    $(".qty-spinner > .btn-decrease").on("mouseleave", function () {
        $(this).stop(true, true).animate({
            "margin-right": "-16px"
        }).css({
            "text-align": "left",
            "padding-left": "4px"
        });
    });

    /*button increase*/
    $(".qty-spinner > .btn-increase").on("mouseenter", function () {
        $(this).stop(true, true).animate({
            "margin-left": "-1px"
        }).css({
            "text-align": "center",
            "padding-right": "7px"
        });
    });

    $(".qty-spinner > .btn-increase").on("mouseleave", function () {
        $(this).stop(true, true).animate({
            "margin-left": "-16px"
        }).css({
            "text-align": "right",
            "padding-right": "4px"
        });
    });

    /*ajax button decrease*/
    $(".qty-spinner > .btn-decrease").on("click", function () {
        var inputQty = $(this).next();
        var val = parseInt(inputQty.val());

        if (val > 1) {
            var proId = $(this).parent().attr("data-proId");
            var parent = $(this).parent().parent();
            var amount = parent.next().children().children(".price");
            var price = unformatNumber(parent.prev().children().children(".price").text());

            $.post(urlAdd, { ProductId: proId, Quantity: -1, Price: price }, function (data) {
                inputQty.val(val - 1);
                amount.text(formatNumber(unformatNumber(amount.text()) - price));

                var cart = $("#mini_cart_block");
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                var total = formatNumber(data.Total);
                subTotal.text(total);
                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

                $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
                $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
            });
        }
    });

    /*ajax button increase*/
    $(".qty-spinner > .btn-increase").on("click", function () {
        var inputQty = $(this).prev();
        var val = parseInt(inputQty.val());

        if (val < parseInt(inputQty.attr("max"))) {
            var proId = $(this).parent().attr("data-proId");
            var parent = $(this).parent().parent();
            var amount = parent.next().children().children(".price");
            var price = unformatNumber(parent.prev().children().children(".price").text());

            $.post(urlAdd, { ProductId: proId, Quantity: 1, Price: price }, function (data) {
                inputQty.val(val + 1);
                amount.text(formatNumber(parseInt(unformatNumber(amount.text())) + parseInt(price)));

                var cart = $("#mini_cart_block");
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                var total = formatNumber(data.Total);
                subTotal.text(total);
                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

                $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
                $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
            });
        }
    });
});