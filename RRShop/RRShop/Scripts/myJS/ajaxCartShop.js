﻿$(document).ready(function () {
    $(".btn-remove2").on("click", function () {
        var proId = $(this).attr("data-proId");
        var price = unformatNumber($(this).parent().prevAll(".unitPrice").children().children().text());
        var tr = $(this).parent().parent();

        $.post(urlRemove, { "ProductId": proId }, function (data) {
            tr.remove();
            var cart = $("#mini_cart_block");
            var quantity = cart.find(".count-item");
            quantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

            $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
            $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
        });
    });

    $("#empty_cart_button").on("click", function () {
        var div = $(this).parents(".col-main.cart");

        $.post(urlRemoveAll, function (data) {
            div.children().remove();
            var cart = $("#mini_cart_block");
            var quantity = cart.find(".count-item");
            quantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");
        });
    });

    $(".unitQuantity > .qty-spinner > input.qty").on("blur", function () {
        var $this = $(this);
        var quantity = $this.val() > $this.attr("max") ? parseInt($this.attr("max")) : $this.val();

        var proId = $this.parent().attr("data-proId");
        var price = unformatNumber($this.parents(".unitQuantity").prev().children().children().text());
        var amount = $this.parents(".unitQuantity").next().children().children();

        $.post(urlChange, { ProductId: proId, Quantity: quantity, Price: price }, function (data) {
            if (quantity == 0) {
                $this.parents("tr").remove();
            } else {
                amount.text(formatNumber(quantity * parseInt(price)));
            }

            var cart = $("#mini_cart_block");
            var cartQuantity = cart.find(".count-item");
            cartQuantity.text(data.CartTotal);

            var subTotal = cart.find(".top-subtotal > span.price > span");
            var total = formatNumber(data.Total);
            subTotal.text(total);
            cartQuantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");

            $("#shopping-cart-totals-table #total > .a-right > strong > .price > span").text(total);
            $("#shopping-cart-totals-table #subTotal > .a-right > .price > span").text(total);
        });
    });
});