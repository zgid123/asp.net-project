﻿/**
*	@name							mobilemenu
*	@descripton						This $jq plugin makes creating mobilemenus pain free
*	@version						1.3
*	@requires						$jq 1.2.6+
*
*	@author							Jan Jarfalk
*	@author-email					jan.jarfalk@unwrongest.com
*	@author-website					http://www.unwrongest.com
*
*	@licens							MIT License - http://www.opensource.org/licenses/mit-license.php
*/

(function ($) {
    $.fn.extend({
        mobilemenu: function () {
            return this.each(function () {

                var $jqul = $(this);

                if ($jqul.data('accordiated'))
                    return false;

                $.each($jqul.find('ul, li>div'), function () {
                    $(this).data('accordiated', true);
                    $(this).hide();
                });

                $.each($jqul.find('span.head'), function () {
                    $(this).click(function (e) {
                        activate(this);
                        return void (0);
                    });
                });

                var active = (location.hash) ? $(this).find('a[href=' + location.hash + ']')[0] : '';

                if (active) {
                    activate(active, 'toggle');
                    $(active).parents().show();
                }

                function activate(el, effect) {
                    $(el).parent('li').toggleClass('active').siblings().removeClass('active').children('ul, div').slideUp('fast');
                    $(el).siblings('ul, div')[(effect || 'slideToggle')]((!effect) ? 'fast' : null);
                }

            });
        }
    });
})($);

$(document).ready(function () {

    $("ul.mobilemenu li.parent").each(function () {
        $(this).append('<span class="head"><a href="javascript:void(0)"></a></span>');
    });

    $('ul.mobilemenu').mobilemenu();

    $("ul.mobilemenu li.active").each(function () {
        $(this).children().next("ul").css('display', 'block');
    });

    //mobile
    $('.btn-navbar').click(function () {

        var chk = 0;
        if ($('#navbar-inner').hasClass('navbar-inactive') && (chk == 0)) {
            $('#navbar-inner').removeClass('navbar-inactive');
            $('#navbar-inner').addClass('navbar-active');
            $('#ma-mobilemenu').css('display', 'block');
            chk = 1;
        }
        if ($('#navbar-inner').hasClass('navbar-active') && (chk == 0)) {
            $('#navbar-inner').removeClass('navbar-active');
            $('#navbar-inner').addClass('navbar-inactive');
            $('#ma-mobilemenu').css('display', 'none');
            chk = 1;
        }
        //$('#ma-mobilemenu').slideToggle();
    });

});
