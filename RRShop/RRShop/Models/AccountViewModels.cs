﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RRShop.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập email")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Duy trì đăng nhập")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Họ tên")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [EmailAddress(ErrorMessage = "Email không đúng")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Số điện thoại")]
        [RegularExpression(@"^\b\d{3}[-. ]?\d{7}[-. ]?\d{0,1}\b$", ErrorMessage = "Số điện thoại không hợp lệ.")]
        [StringLength(11, ErrorMessage = "Số điện thoại phải từ 7 đến 11 số.", MinimumLength = 7)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Chứng minh nhân dân")]
        [RegularExpression(@"^\b\d{9}(?:\d{2})?\b$", ErrorMessage = "Chứng minh nhân dân phải có 9 hoặc 11 số.")]
        public string PersonalIdentity { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Ngày sinh")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [Display(Name = "Giới tính")]
        public bool Gender { get; set; }

        [Required(ErrorMessage = "Đây là thông tin bắt buộc")]
        [MinLength(6, ErrorMessage = "{0} phải có ít nhất 6 ký tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu xác nhận")]
        [Compare("Password", ErrorMessage = "Mật khẩu xác nhận không đúng.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class InformationViewModels
    {
        [Required(ErrorMessage = "Họ tên không được bỏ trống.")]
        [Display(Name = "Họ tên")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email không được bỏ trống.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được bỏ trống.")]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Chứng minh nhân dân không được bỏ trống.")]
        [Display(Name = "Chứng minh nhân dân")]
        public string PersonalIdentity { get; set; }

        [Display(Name = "Ngày sinh")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Giới tính")]
        public bool Gender { get; set; }

        [Required(ErrorMessage = "Số điện thoại không được bỏ trống.")]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }
    }
}
