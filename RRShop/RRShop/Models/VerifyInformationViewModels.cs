﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RRShop.Models
{
    public class VerifyAddressViewModels
    {
        [Required(ErrorMessage = "Phải nhập địa chỉ")]
        public string Address { get; set; }
    }
}