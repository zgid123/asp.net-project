﻿using Business.AdminBUS;
using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class InvoiceController : Controller
    {
        // GET: Admin/Invoice
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult LoadInvoices(short? page)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            return Json(InvoiceAdmin.LoadAllInvoice(page.Value), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateInvoice(Invoice In)
        {
            if(In.InvoiceID > 0)
            {
                IEnumerable<InvoiceDetailViewModel> items = InvoiceAdmin.LoadInvoiceDetailsByInvoiceID(In.InvoiceID);
                if (!InvoiceAdmin.IsInvoiceDelivered(In.InvoiceID))
                {
                    if (InvoiceAdmin.IDIsStatusDelivered(In.StatusID))
                    {
                        foreach(var item in items)
                        {
                            InvoiceAdmin.UpdateSoldProduct(item.ProductID, item.Quantity);
                        }
                    }
                }
                else
                {
                    if (!InvoiceAdmin.IDIsStatusDelivered(In.StatusID))
                    {
                        foreach (var item in items)
                        {
                            InvoiceAdmin.UpdateSoldProduct(item.ProductID, -item.Quantity);
                        }
                    }
                }

                return Json(InvoiceAdmin.UpdateInvoice(In), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult LoadInvoiceDetails(int InvoiceID)
        {
            if(InvoiceID > 0)
            {
                return Json(InvoiceAdmin.LoadInvoiceDetailsByInvoiceID(InvoiceID), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveInvoiceDetail(InvoiceDetail InD)
        {
            if (!InvoiceAdmin.IsInvoiceDelivered(InD.InvoiceID))
            {
                if (InvoiceAdmin.DeleteInvoiceDetail(InD.InvoiceDetailID))
                {
                    InvoiceAdmin.UpdateQuantityProduct(InD.ProductID, InD.Quantity);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                    
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult LoadStatuses()
        {
            return Json(InvoiceAdmin.LoadAllStatuses(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateStatus(Status status)
        {
            if(status.StatusID > 0)
            {
                return Json(InvoiceAdmin.UpdateStatus(status), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(true)]
        [HttpPost]
        public ActionResult InsertStatus(Status status)
        {
            if (status.StatusName != null)
            {
                return Json(InvoiceAdmin.InsertStatus(status), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}