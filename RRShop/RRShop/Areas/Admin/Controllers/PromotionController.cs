﻿using Business.AdminBUS;
using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PromotionController : Controller
    {
        // GET: Admin/Promotion
        public ActionResult Index()
        {
            return View();
        }



        //Http angularJS
        [ValidateInput(true)]
        [HttpPost]
        public ActionResult CreateSale(Sale sale)
        {
            if (sale.StartDate != null && sale.DueDate != null && sale.DueDate >= sale.StartDate)
            {
                return Json(PromotionAdmin.AddSale(sale), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public ActionResult LoadSales(short? page)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            return Json(PromotionAdmin.LoadAllSale(page.Value), JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(true)]
        [HttpPost]
        public ActionResult UpdateSale(Sale sale)
        {
            if (sale.SaleID != 0)
            {
                if (sale.StartDate != null && sale.DueDate != null && sale.StartDate <= sale.DueDate)
                {
                    return Json(PromotionAdmin.UpdateSale(sale), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        //Detail
        //[ValidateInput(false)]
        public ActionResult Detail(short? SaleID)
        {
            if (SaleID.HasValue)
            {
                ViewBag.SaleID = SaleID.Value;
            }
            //SearchItems s = new SearchItems();
            //s.CategoryID = 4;
            return View();
        }

        [ValidateInput(false)]
        public ActionResult LoadSaleDetail(short SaleID, short? page)
        {
            if (SaleID == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            if (!page.HasValue)
            {
                page = 1;
            }

            return Json(PromotionAdmin.LoadSaleDetailByID(SaleID, page.Value), JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Searching(SearchItems s)
        {
            return Json(PromotionAdmin.SearchForInsertSale(s), JsonRequestBehavior.AllowGet);
            //return Json(false, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult InsertProductToSD(int saleID, int[] proIDArray, int[] discountIDArray)
        {
            if(saleID > 0)
            {
                for(int i = 0; i < proIDArray.Length; i++)
                {
                    SaleDetail sd = new SaleDetail();
                    sd.ProductID = proIDArray[i];
                    sd.SaleID = saleID;
                    if (discountIDArray[i] < 0)
                        continue;

                    sd.DiscountID = (byte)discountIDArray[i];
                    PromotionAdmin.InsertSaleDetail(sd);
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateSaleDetail(SaleDetail sd)
        {
            if (sd.SaleDetailID > 0)
            {
                return Json(PromotionAdmin.UpdateSaleDetail(sd), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveSaleDetail(int SaleDetailID)
        {
            if (SaleDetailID > 0)
            {
                return Json(PromotionAdmin.DeleteSaleDetail(SaleDetailID), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        //----------Discount-------------------------------
        [ValidateInput(false)]
        public ActionResult LoadDiscount()
        {
            return Json(PromotionAdmin.LoadAllDiscount(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateDiscount(double discount)
        {
            if(discount != 0.0)
            {
                Discount d = new Discount();
                d._Discount = discount;
                return Json(PromotionAdmin.InsertDiscount(d), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult RemoveDiscount(int discountID)
        {
            if (discountID > 0)
            {
                return Json(PromotionAdmin.DeleteDiscount(discountID), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDiscount(Discount discount)
        {
            if (discount.DiscountID > 0 && discount.DiscountID >= 0 && discount._Discount < 1)
            {
                return Json(PromotionAdmin.UpdateDiscount(discount), JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}