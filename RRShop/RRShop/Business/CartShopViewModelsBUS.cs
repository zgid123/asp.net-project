﻿using AutoMapper;
using Business.Business;
using Business.ViewModels;
using Connection;
using RRShop.ApiModels;
using RRShop.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRShop.Business
{
    public class CartShopViewModelsBUS
    {
        public static CartShopViewModels RemoveItem(int proId)
        {
            CartShopViewModels currentCart = CartShopViewModels.GetCurrentCart();

            List<CartShopSqlView> list = currentCart.CartItems;

            CartShopSqlView item = list.SingleOrDefault(c => c.ProductId == proId);

            if (item != null)
            {
                currentCart.CartTotal -= item.Quantity;
                currentCart.Total -= item.Quantity * item.Price;

                list.Remove(item);
                CartShopSqlViewBUS.Delete(currentCart.CartID, proId);
            }

            return currentCart;
        }

        public static CartShopViewModels AddOrUpdateItem(CartItemApiModels item)
        {
            CartShopViewModels currentCart = CartShopViewModels.GetCurrentCart();

            List<CartShopSqlView> list = currentCart.CartItems;

            CartShopSqlView cart = list.SingleOrDefault(c => c.ProductId == item.ProductID);

            var cartItem = Mapper.Map<CartShop>(item);
            cartItem.UserID = currentCart.UserId;
            cartItem.CartShopID = currentCart.CartID;

            if (cart == null)
            {
                cart = new CartShopSqlView
                {
                    ProductId = item.ProductID,
                    Quantity = item.Quantity
                };

                list.Add(cart);

                CartShopSqlViewBUS.Insert(cartItem);
            }
            else
            {
                cart.Quantity += item.Quantity;

                cartItem.Quantity = cart.Quantity;
                CartShopSqlViewBUS.Update(cartItem);
            }

            currentCart.CartTotal += item.Quantity;
            currentCart.Total += item.Quantity * item.Price;

            return currentCart;
        }

        public static CartShopViewModels RemoveAllItems()
        {
            CartShopViewModels cart = CartShopViewModels.GetCurrentCart();
            cart.CartItems.Clear();
            cart.Total = 0;
            cart.CartTotal = 0;

            CartShopSqlViewBUS.DeleteAll(cart.CartID);

            return cart;
        }

        public static CartShopViewModels UpdateChange(CartItemApiModels item)
        {
            CartShopViewModels currentCart = CartShopViewModels.GetCurrentCart();

            List<CartShopSqlView> list = currentCart.CartItems;

            CartShopSqlView cart = list.SingleOrDefault(c => c.ProductId == item.ProductID);

            if (item.Quantity == 0)
            {
                currentCart.CartTotal -= cart.Quantity;
                currentCart.Total -= cart.Quantity * item.Price;

                list.Remove(cart);
                CartShopSqlViewBUS.Delete(currentCart.CartID, cart.ProductId);
            }
            else
            {
                short quantity = (short)(item.Quantity - cart.Quantity);

                cart.Quantity += quantity;

                var cartItem = Mapper.Map<CartShop>(item);
                cartItem.UserID = currentCart.UserId;
                cartItem.CartShopID = currentCart.CartID;
                cartItem.Quantity = cart.Quantity;

                CartShopSqlViewBUS.Update(cartItem);

                currentCart.CartTotal += quantity;
                currentCart.Total += quantity * item.Price;
            }

            return currentCart;
        }
    }
}
