﻿using Business.Business;
using Business.ViewModels;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    public class MenuController : Controller
    {
        public ActionResult _MenuPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new MenuViewModels());
        }

        public ActionResult _MobileMenuPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new MenuViewModels());
        }

        public ActionResult _FooterMenuPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(CategoryViewModelsBUS.GetCategories());
        }

        public ActionResult _LeftMenuPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
    }
}