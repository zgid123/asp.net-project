﻿using Business.AdminBUS;
using Business.Business;
using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("CatalogSearch")]
    public class CatalogSearchController : Controller
    {
        [Route("")]
        // GET: CatalogSearch
        public ActionResult Index()
        {
            return View();
        }

        [Route("Result")]
        public ActionResult Result(int? page, string keyword = "")
        {
            if (keyword == "")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.keyword = keyword;
            return View(Searching.NormalSearch(keyword, page));
        }


        [Route("AdvancedResult")]
        public ActionResult AdvancedResult(SearchItems item, int? page)
        {
            ViewBag.SearchItems = item;
            return View(Searching.AdvancedSearch(item, page));
        }
        public ActionResult _CategoryList()
        {
            Category cat = new Category();

            IEnumerable<Category> IlistCat = CategoryBUS.GetCategories();
            List<SelectListItem> listCat = new List<SelectListItem>();

            listCat.Add(new SelectListItem {
                Text = "-- Chọn loại --",
                Value = "-1"
            });

            foreach (var items in IlistCat)
            {
                listCat.Add(new SelectListItem
                {
                    Text = items.CategoryName,
                    Value = items.CategoryID.ToString(),
                });
            }
            ViewBag.listCat = listCat;

            return View(cat);
        }

        public ActionResult _ProducerList()
        {

            Producer producer = new Producer();

            IEnumerable<ProducerViewModels> IlistProducer = ProducerViewModelsBUS.GetProducers();
            List<SelectListItem> listProducer = new List<SelectListItem>();

            listProducer.Add(new SelectListItem
            {
                Text = "-- Chọn NSX --",
                Value = "-1"
            });

            foreach (var items in IlistProducer)
            {
                listProducer.Add(new SelectListItem
                {
                    Text = items.ProducerName,
                    Value = items.ProducerID.ToString()
                });
            }
            ViewBag.listProducer = listProducer;
            return View(producer);
        }

        public ActionResult AjaxGetSub(int CatID)
        {
            return Json(SubCategoryAdmin.GetSubCategoryByCategory(CatID), JsonRequestBehavior.AllowGet);
        }
    }
}