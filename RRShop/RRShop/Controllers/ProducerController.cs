﻿using Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Producer")]
    public class ProducerController : Controller
    {
        [Route("Index/{page?}")]
        public ActionResult Index(int? page)
        {
            return View(ProducerViewModelsBUS.GetProducersByProducer(page));
        }

        [Route("{id}/{name}/{page?}")]
        public ActionResult Producer(int? page, short id = 0, string name = "")
        {
            if (id == 0 || name == "")
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Producer = ProducerViewModelsBUS.GetProducerNameById(id);

            return View(ProductViewModelsBUS.GetProductsByProducer(id, page));
        }
    }
}