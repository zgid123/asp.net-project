﻿using Business.Business;
using Connection;
using RRShop.Business;
using RRShop.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    public class ShoppingCartController : Controller
    {
        [Route("ShoppingCart/{page?}")]
        public ActionResult Index(short? page)
        {
            ViewBag.Cart = CartShopViewModels.GetCurrentCart();

            return View(CartProductViewModelsBUS.GetCartProducts(page, ViewBag.Cart.CartItems));
        }

        [Route("ShoppingCart/Partial")]
        public ActionResult _CartShopPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(CartShopViewModels.GetCurrentCart());
        }

        [Route("ShoppingCart/Checkout")]
        [Authorize]
        public ActionResult Checkout()
        {
            if (!CartShopSqlViewBUS.HasCartItem(Request.Cookies["CartShop"].Value))
            {
                return RedirectToAction("Index", "ShoppingCart");
            }

            ViewBag.User = IdentityModelsBUS.GetUserById(Request.Cookies["UserId"].Value);
            var cart = CartShopViewModels.GetCurrentCart();
            ViewBag.InvalidCart = cart.CartItems.Where(c => c.RealQuantity - c.Quantity < 0).Count();
            cart.Total -= (long)cart.CartItems.Where(c => c.RealQuantity == 0).Sum(c => (c.Quantity - c.RealQuantity) * c.Price);
            cart.CartTotal -= (short)ViewBag.InvalidCart;
            ViewBag.CartShop = cart;

            return View(new VerifyAddressViewModels());
        }
    }
}