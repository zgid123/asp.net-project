namespace RRShop.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System.Data.Entity.Migrations;
    using System.Security.Claims;
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Roles.AddOrUpdate(
                r => r.Name,
                new IdentityRole { Name = "Admin" },
                new IdentityRole { Name = "Member" }
            );

            using (var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
            {
                var adminAccount = new ApplicationUser()
                {
                    UserName = "admin@rrshop.com",
                    Email = "admin@rrshop.com",
                    Name = "Admin",
                    IsRemoved = false
                };

                var result = UserManager.Create(adminAccount, "RRShop@Com");

                if (result.Succeeded)
                {
                    UserManager.AddToRole(adminAccount.Id, "Admin");

                    UserManager.AddClaim(adminAccount.Id, new Claim("RealName", adminAccount.Name));
                }
            }

            base.Seed(context);
        }
    }
}
