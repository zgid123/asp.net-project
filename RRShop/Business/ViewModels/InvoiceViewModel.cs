﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class InvoiceViewModel : Invoice
    {
        [Column] public string StatusName { get; set; }
        [Column] public string Name { get; set; }
    }
}
