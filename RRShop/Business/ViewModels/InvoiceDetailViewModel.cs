﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class InvoiceDetailViewModel : InvoiceDetail
    {
        [Column] public string ProductName { get; set; }
    }
}
