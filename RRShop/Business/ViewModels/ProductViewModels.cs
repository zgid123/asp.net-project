﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class ProductViewModels
    {
        [Column]
        public int ProductID { get; set; }

        [Column]
        public int CategoryID { get; set; }

        [Column]
        public int SubCategoryID { get; set; }


        [Column]
        public int ProducerID { get; set; }

        [Column]
        public string ProductName { get; set; }

        [Column]
        public string RouteName { get; set; }

        [Column]
        public int SalePrice { get; set; }

        [Column]
        public int Sold { get; set; }

        [Column]
        public int Price { get; set; }

        [Column]
        public int Viewed { get; set; }

        [Column]
        public int Quantity { get; set; }

        [Column]
        public string Description { get; set; }

        [Column]
        public string ProducerName { get; set; }

        [Column]
        public string CategoryName { get; set; }

        [Column]
        public DateTime UploadDate { get; set; }

        [Column]
        public string CategoryRouteName { get; set; }

        [Column]
        public string SubCategoryName { get; set; }

        [Column]
        public string SubCategoryRouteName { get; set; }

        [Column]
        public DateTime? DueDate { get; set; }

        public List<Image> Images { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
