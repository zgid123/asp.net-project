﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class CommentSqlView
    {
        [Column]
        public string UserName { get; set; }

        [Column]
        public string Comment { get; set; }

        [Column]
        public DateTime PostedDate { get; set; }
    }
}
