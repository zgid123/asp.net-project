﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class SubCategoryViewModels
    {
        [Column]
        public byte SubCategoryID { get; set; }
        
        [Column]
        public string SubCategoryName { get; set; }

        [Column]
        public byte CategoryID { get; set; }

        [Column]
        public string CategoryName { get; set; }

        [Column]
        public string RouteName { get; set; }
    }
}
