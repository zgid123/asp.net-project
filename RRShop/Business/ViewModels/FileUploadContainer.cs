﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Business.ViewModels
{
    public class FileUploadContainer
    {
        public int ProductID { get; set; }
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
    }
}
