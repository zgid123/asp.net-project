﻿using Business.Business;
using System.Collections.Generic;

namespace Business.ViewModels
{
    public class MenuViewModels
    {
        public List<MenuCategoryViewModels> Categories { get; set; }
        public IEnumerable<ProducerViewModels> Producers { get; set; }

        public MenuViewModels()
        {
            Categories = MenuCategoryViewModelsBUS.GetCategories();
            Producers = ProducerViewModelsBUS.GetProducers();
        }
    }
}
