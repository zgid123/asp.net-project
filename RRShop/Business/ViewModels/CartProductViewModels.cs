﻿using Connection;
using PetaPoco;
using System.Collections.Generic;

namespace Business.ViewModels
{
    public class CartProductViewModels
    {
        [Column]
        public int ProductID { get; set; }

        [Column]
        public string ProductName { get; set; }

        [Column]
        public string RouteName { get; set; }

        [Column]
        public int SalePrice { get; set; }

        [Column]
        public int Price { get; set; }

        public ushort Quantity { get; set; }

        [Column]
        public ushort RealQuantity { get; set; }

        public List<Image> Images { get; set; }
    }
}
