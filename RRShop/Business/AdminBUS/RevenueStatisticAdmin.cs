﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class RevenueStatisticAdmin
    {
        public static int statisticsForDate(string date)
        {
            var db = new ConnectionDB();
            string queryString = @"Select SUM(InD.Quantity) 
                                    From Invoice Inv, InvoiceDetail InD, Status S
                                    Where Inv.InvoiceID = InD.InvoiceID And
                                            CAST(Inv.Date AS DATE) = @0 And 
		                                    Inv.StatusID = S.StatusID And
		                                    S.StatusName = N'Đã giao'";
            string result = db.SingleOrDefault<string>(queryString, date);
            int count = 0;
            if(result != null)
            {
                count = int.Parse(result);
            }

            return count;
        }

        public static int statisticsForMonth(short month, short year)
        {
            var db = new ConnectionDB();
            string queryString = @"Select SUM(InD.Quantity) 
                                    From Invoice Inv, InvoiceDetail InD, Status S
                                    Where Inv.InvoiceID = InD.InvoiceID And
                                            MONTH(Inv.Date) = @0 And YEAR(Inv.Date) = @1 And 
		                                    Inv.StatusID = S.StatusID And
		                                    S.StatusName = N'Đã giao'";
            string result = db.SingleOrDefault<string>(queryString, month, year);
            int count = 0;
            if (result != null)
            {
                count = int.Parse(result);
            }
            return count;
        }

        public static int statisticsForYear(short year)
        {
            var db = new ConnectionDB();
            string queryString = @"Select SUM(InD.Quantity) 
                                    From Invoice Inv, InvoiceDetail InD, Status S
                                    Where Inv.InvoiceID = InD.InvoiceID And
                                            YEAR(Inv.Date) = @0 And 
		                                    Inv.StatusID = S.StatusID And
		                                    S.StatusName = N'Đã giao'";
            string result = db.SingleOrDefault<string>(queryString, year);
            int count = 0;
            if (result != null)
            {
                count = int.Parse(result);
            }
            return count;
        }

        public static IEnumerable<StatisticCatViewModel> statisticsForProducer()
        {
            var db = new ConnectionDB();
            string queryString = @"Select PC.ProducerName, data.Quantity
                                    From Producer PC 
		                                    LEFT JOIN (
			                                    Select PC.ProducerID, PC.ProducerName, SUM(InD.Quantity) AS 'Quantity'
			                                    From Producer PC, Product P, Invoice Inv, InvoiceDetail InD, Status S
			                                    Where PC.ProducerID = P.ProducerID And
					                                    InD.ProductID = P.ProductID And
					                                    InD.InvoiceID = Inv.InvoiceID And
					                                    Inv.StatusID = S.StatusID And
					                                    S.StatusName = N'Đã giao'
			                                    Group By PC.ProducerID, PC.ProducerName) data
		                                    ON PC.ProducerID = data.ProducerID";
            var list = db.Query<StatisticCatViewModel>(queryString);
            return list;
        }

        public static IEnumerable<StatisticCatViewModel> statisticsForCategory()
        {
            var db = new ConnectionDB();
            string queryString = @"Select Cat.CategoryName, data.Quantity
                                    From Category Cat 
		                                    LEFT JOIN (
			                                    Select Cat.CategoryID, Cat.CategoryName, SUM(InD.Quantity) AS 'Quantity'
			                                    From Category Cat, Product P, Invoice Inv, InvoiceDetail InD, Status S
			                                    Where Cat.CategoryID = P.CategoryID And
					                                    InD.ProductID = P.ProductID And
					                                    InD.InvoiceID = Inv.InvoiceID And
					                                    Inv.StatusID = S.StatusID And
					                                    S.StatusName = N'Đã giao'
			                                    Group By Cat.CategoryID, Cat.CategoryName) data
		                                    ON Cat.CategoryID = data.CategoryID";
            var list = db.Query<StatisticCatViewModel>(queryString);
            return list;
        }
    }
}
