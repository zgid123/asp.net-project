﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class InvoiceAdmin
    {
        public static Page<InvoiceViewModel> LoadAllInvoice(short page)
        {
            var db = new ConnectionDB();
            string queryString = @"Select Inv.InvoiceID, Inv.UserID, U.Name,Inv.Date, Inv.Total, Inv.StatusID, S.StatusName, Inv.Address
                                    From Invoice Inv, Status S, [User] U
                                    Where Inv.StatusID = S.StatusID And
                                            U.Id = Inv.UserID
                                    Order By Date Desc";

            var list = db.Page<InvoiceViewModel>(page, 20, queryString);

            return list;
        }

        public static bool UpdateInvoice(Invoice In)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "StatusID" };
            int row = db.Update(In, In.InvoiceID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static IEnumerable<InvoiceDetailViewModel> LoadInvoiceDetailsByInvoiceID(int InvoiceID)
        {
            var db = new ConnectionDB();
            string queryString = @"Select InD.InvoiceDetailID, InD.InvoiceID, InD.ProductID, P.ProductName, InD.Price, InD.Quantity
                                    From InvoiceDetail InD, Product P
                                    Where InD.ProductID = P.ProductID And
                                            InD.InvoiceID = @0";

            var list = db.Query<InvoiceDetailViewModel>(queryString, InvoiceID);

            return list;
        }

        public static bool DeleteInvoiceDetail(int InvoiceDetailID)
        {
            Object item;
            using (var db = new ConnectionDB())
            {
                item = db.Delete("InvoiceDetail", "InvoiceDetailID", null, InvoiceDetailID);
            }

            if (item != null)
            {
                return true;
            }

            return false;
        }

        public static bool UpdateSoldProduct(int ProductID, int Sold)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "Q" };
            string queryString = @"Update Product
                                    Set Sold = Sold + @0
                                    Where ProductID = @1";
            int row = db.Execute(queryString, Sold, ProductID);

            if (row == 0)
                return false;

            return true;
        }

        public static bool UpdateQuantityProduct(int ProductID, int Quantity)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "Q" };
            string queryString = @"Update Product
                                    Set Quantity = Quantity + @0
                                    Where ProductID = @1";
            int row = db.Execute(queryString, Quantity, ProductID);

            if (row == 0)
                return false;

            return true;
        }

        public static bool IsInvoiceDelivered(int InvoiceID)
        {
            var db = new ConnectionDB();

            string queryString = @"Select 'true'
                                    From Invoice Inv, Status S
                                    Where Inv.StatusID = S.StatusID And
                                            InvoiceID = @0 And 
                                            S.StatusName = N'Đã giao'";
            string status = db.SingleOrDefault<string>(queryString, InvoiceID);

            if (status == "true")
                return true;

            return false;
        }

        public static bool IDIsStatusDelivered(int StatusID)
        {
            var db = new ConnectionDB();

            string queryString = @"Select 'true'
                                    From Status S
                                    Where S.StatusID = @0 And 
                                            S.StatusName = N'Đã giao'";
            string status = db.SingleOrDefault<string>(queryString, StatusID);

            if (status == "true")
                return true;

            return false;
        }

        public static int GetIfInvoiceDelivered(int InvoiceID)
        {
            var db = new ConnectionDB();

            string queryString = @"Select S.StatusID
                                    From Invoice Inv, Status S
                                    Where Inv.StatusID = S.StatusID And
                                            InvoiceID = @0 And 
                                            S.StatusName = N'Đã giao'";
            string status = db.SingleOrDefault<string>(queryString, InvoiceID);

            if (status != null)
                return int.Parse(status);

            return 0;
        }

        public static IEnumerable<Status> LoadAllStatuses()
        {
            var db = new ConnectionDB();
            string queryString = @"Select StatusID, StatusName
                                    From Status";

            var list = db.Query<Status>(queryString);

            return list;
        }

        public static bool InsertStatus(Status status)
        {
            Object item;
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Status", "StatusID", status);
            }

            if (item != null)
            {
                return true;
            }

            return false;
        }

        public static bool UpdateStatus(Status status)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "StatusName" };
            int row = db.Update(status, status.StatusID, str);

            if (row == 0)
                return false;

            return true;
        }
    }
}
