﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class UserAdmin
    {
        public static Page<UserViewModel> GetAllUsers(int page)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT Id, Email, Name, Address, PersonalIdentity, PhoneNumber, DateOfBirth, Gender
                                    FROM [User]
                                    WHERE IsRemoved = 0";
            var list = db.Page<UserViewModel>(page, 20, queryString);

            return list;
        }

        public static UserViewModel GetUserById(string id)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT Id, Email, Name, Address, PersonalIdentity, PhoneNumber, DateOfBirth, Gender
                                    FROM [User]
                                    WHERE IsRemoved = 0 And Id = @0";
            UserViewModel user = db.SingleOrDefault<UserViewModel>(queryString, id);

            return user;
        }

        public static bool UpdateUser(UserViewModel user)
        {
            var db = new ConnectionDB();

            IEnumerable<string> col = new string[] {"Name", "Address", "PersonalIdentity", "PhoneNumber", "DateOfBirth", "Gender" };
            int row = db.Update("User", "Id", user, user.Id, col);
            //int row = db.Update(AspNetUsers, user.Id, str);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteUser(string id)
        {
            var db = new ConnectionDB();
            string queryString = "UPDATE [User] SET IsRemoved = 1 Where Id =@0";
            int row = db.Execute(queryString, id);

            if (row == 0)
                return false;

            return true;
        }
    }
}
