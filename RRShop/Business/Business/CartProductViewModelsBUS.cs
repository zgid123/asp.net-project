﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class CartProductViewModelsBUS
    {
        public static Page<CartProductViewModels> GetCartProducts(short? page, List<CartShopSqlView> items)
        {
            Page<CartProductViewModels> result;

            if (items == null || items.Count == 0)
            {
                result = new Page<CartProductViewModels>();
            }
            else
            {
                using (var db = new ConnectionDB())
                {
                    string query = @"Select P.ProductID, ProductName, Price, RouteName, Quantity, Price - Price * D.Discount as SalePrice , P.Quantity as RealQuantity
                                     From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                     Where ((S.StartDate is null AND S.DueDate is null) 
                                           OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                           AND P.IsRemoved = 0
                                           AND P.ProductID IN (@proID)";

                    result = db.Page<CartProductViewModels>(page.HasValue ? page.Value : 1, 10, query, new { proID = items.Select(c => c.ProductId).ToList() });

                    foreach (var element in result.Items)
                    {
                        element.Images = ImageBUS.GetImageByProduct(element.ProductID);

                        var item = items.Single<CartShopSqlView>(c => c.ProductId == element.ProductID);
                        element.Quantity = (ushort)item.Quantity;
                        element.RealQuantity = (ushort)item.RealQuantity;
                    }
                }
            }

            return result;
        }

        public static uint GetPriceById(int id)
        {
            uint result = 0;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Price, Price - Price * D.Discount as SalePrice 
                                 From Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID
                                 Where ((S.StartDate is null AND S.DueDate is null) 
                                       OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))
                                       AND P.IsRemoved = 0
                                       AND P.ProductID = @0";

                var product = db.SingleOrDefault<CartProductViewModels>(query, id);

                result = (uint)(product.SalePrice == 0 ? product.Price : product.SalePrice);
            }

            return result;
        }
    }
}
