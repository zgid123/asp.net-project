﻿using Business.ViewModels;
using Connection;
using System.Collections.Generic;

namespace Business.Business
{
    public class CartShopSqlViewBUS
    {
        public static IEnumerable<CartShopSqlView> GetCartShopById(string id)
        {
            IEnumerable<CartShopSqlView> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select C.ProductID, C.Quantity, Price - (ISNULL(D.Discount, 1) * P.Price) as Price, P.Quantity as RealQuantity, P.ProductName
                                 From CartShop C, Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID 
                                 Where CartShopID = @0 
                                       AND C.ProductID = P.ProductID
                                       AND ((S.StartDate is null AND S.DueDate is null) 
                                           OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))";

                result = db.Query<CartShopSqlView>(query, id);
            }

            return result;
        }

        public static IEnumerable<CartShopSqlView> GetCartShopByUserId(string id)
        {
            IEnumerable<CartShopSqlView> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select C.ProductID, C.Quantity, Price - (ISNULL(D.Discount, 1) * P.Price) as Price, P.Quantity as RealQuantity, P.ProductName
                                 From CartShop C, Product P LEFT JOIN SaleDetail DS LEFT JOIN Discount D ON DS.DiscountID = D.DiscountID LEFT JOIN Sale S ON DS.SaleID = S.SaleID ON P.ProductID = DS.ProductID 
                                 Where UserID = @0 
                                       AND C.ProductID = P.ProductID
                                       AND ((S.StartDate is null AND S.DueDate is null) 
                                           OR (GETDATE() >= S.StartDate AND GETDATE() <= S.DueDate))";

                result = db.Query<CartShopSqlView>(query, id);
            }

            return result;
        }

        public static void Delete(string cartId, int proId)
        {
            using (var db = new ConnectionDB())
            {
                string query = "Delete From CartShop Where CartShopID = @0 AND ProductID = @1";

                db.Execute(query, cartId, proId);
            }
        }

        public static void DeleteAll(string cartId)
        {
            using (var db = new ConnectionDB())
            {
                string query = "Delete From CartShop Where CartShopID = @0";

                db.Execute(query, cartId);
            }
        }

        public static void Insert(CartShop cart)
        {
            using (var db = new ConnectionDB())
            {
                string query = @"Insert into CartShop(CartShopID, ProductID, UserID, Quantity) 
                                 Values(@0, @1, @2, @3)";

                db.Execute(query, cart.CartShopID, cart.ProductID, cart.UserID, cart.Quantity);
            }
        }

        public static void Update(CartShop cart)
        {
            using (var db = new ConnectionDB())
            {
                string query = @"Update CartShop 
                                 Set Quantity = @0 
                                 Where CartShopID = @1 AND ProductID = @2";

                db.Execute(query, cart.Quantity, cart.CartShopID, cart.ProductID);
            }
        }

        public static void MergeCart(string cartId, string userId, List<CartShopSqlView> listCart)
        {
            using (var db = new ConnectionDB())
            {
                string query = @"Delete From CartShop
                                 Where UserID = @0 AND ProductID = @1 AND CartShopID != @2";

                foreach (var item in listCart)
                {
                    db.Execute(query, userId, item.ProductId, cartId);
                }

                query = @"Update CartShop
                              Set CartShopID = @0
                              Where UserID = @1 AND CartShopID != @0";
                db.Execute(query, cartId, userId);

                query = @"Update CartShop
                              Set UserID = @0
                              Where CartShopID = @1";
                db.Execute(query, userId, cartId);
            }
        }

        public static bool HasCartItem(string cartId)
        {
            int result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Count(CartShopID) From CartShop Where CartShopID = @0";

                result = db.ExecuteScalar<int>(query, cartId);
            }

            return result > 0 ? true : false;
        }

        public static string GetCartShopId(string userId)
        {
            string result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Top 1 CartShopID From CartShop Where UserID = @0";

                result = db.ExecuteScalar<string>(query, userId);
            }

            return result;
        }
    }
}
