﻿using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class Library
    {
        public static bool HasSale()
        {
            bool result;

            using (var db = new ConnectionDB())
            {
                result = db.Query<Product>("Select Top 1 ProductID From Product Where Saled = @0", true)
                           .FirstOrDefault() != null ? true : false;
            }

            return result;
        }
    }
}
