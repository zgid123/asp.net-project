﻿using Connection;
using System.Collections.Generic;

namespace Business.Business
{
    public class CategoryBUS
    {
        /// <summary>
        /// Gets Category List
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Category> GetCategories()
        {
            IEnumerable<Category> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select CategoryID, CategoryName From Category Where IsRemoved = @0";
                result = db.Query<Category>(query, 0);
            }

            return result;
        }
    }
}
