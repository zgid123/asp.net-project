﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class SubCategoryViewModelsBUS
    {
        /// <summary>
        /// Gets Sub Category List based on Category
        /// </summary>
        /// <param name="categoryID">Category ID</param>
        /// <returns></returns>
        public static IEnumerable<SubCategoryViewModels> GetSubCategoriesByCategory(int categoryID)
        {
            IEnumerable<SubCategoryViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select SubCategoryID, SubCategoryName, RouteName
                                 From SubCategory
                                 Where CategoryID = @0
                                       AND IsRemoved = @1";

                result = db.Query<SubCategoryViewModels>(query, categoryID, 0);
            }

            return result;
        }

        /// <summary>
        /// Gets Sub Category based on Sub Category's Id
        /// </summary>
        /// <param name="id">Sub Category's Id</param>
        /// <returns></returns>
        public static string GetSubCategoryNameById(byte id)
        {
            string result;

            using (var db = new ConnectionDB())
            {
                result = db.SingleOrDefault<SubCategoryViewModels>(@"Select SubCategoryName 
                                                                     From SubCategory
                                                                     Where SubCategoryID = @0", id)
                           .SubCategoryName;
            }

            return result;
        }
    }
}
